\documentclass[]{book}

\usepackage{amssymb}
%\usepackage{natbib}
\usepackage{longtable}

%\include{utilities}

% Title Page
\title{Collection of Light Curve Analysis Metrics from Various Papers}
\author{}

\begin{document}
\maketitle

\tableofcontents

\chapter{Curve Transformations}

\section{Smoothing}
Curve smoothing

\section{Period Folding}
In the case of light curves for periodic variable stars, once the period of the star has been determined, the light curve can then be folded over the period to get a more accurate light curve in terms of phase rather than time.

The phase folding can be achieved by taking the time of each observation, dividing it by the period of the light curve, and taking the remainder of that division. That remainder is the phase of the observation.

\begin{verbatim}
def phase_fold(times, period):
    phase_times = (times % period) / period
    
    return phase_times
\end{verbatim}

Since light curves obtained by surveys are sparse, phase folding, especially when combined with curve smoothing, can help to give a better indication of the actual shape of the light curve.

\section{Slopes}
Some light curve metrics make use of the slopes between observation points. These slopes are used to measure the degrees of change in the light curve, as some stars undergo greater change in less time than others.

The light curve slopes are defined by the following formula.

\begin{equation}
slope_{i} = \frac{mag_{i+1} - mag_{i}}{time_{i+1} - time_{i}}
\end{equation}

The slopes between each point in the light curve with $n$ observations can be computed by creating vectors of the times and magnitudes for observation indexes $[1, n]$ and $[0, n - 1]$ and performing element-wise subtraction followed by element-wise division.

\begin{verbatim}
def curve_slopes(times, magnitudes):
    mag_diffs = magnitudes[1:] - magnitudes[0:-1]
    time_diffs = times[1:] - times[0:-1]
    
    slopes = np.divide(mag_diffs, time_diffs)
    
    return slopes
\end{verbatim}

\section{Observation Cluster Averaging}
When surveys record observations of a star, often they will record several observations around the same time. This leads to clusters of a few points around the same times in the observed light curves.

If this time clustering is present in a light curve, then it is possible to average the points in each cluster together in order to get a less observed, but more accurate light curve.

\chapter{Metrics}

\begin{longtable}{l|l}
	Metric & Reference(s) \\
	\hline
	
	Amplitude & \cite{richards_machine-learned_2011}
	\cite{richards_machine-learned_2011}
	\cite{disanto_analysis_2016}
	\cite{faraway_modeling_2016} \\
	
	Beyond 1std & \cite{richards_machine-learned_2011}
	\cite{disanto_analysis_2016} \\
	
	Consecutive Same-Sign Deviations from the Mean Magnitude & \cite{sokolovsky_comparative_2017} \\
	
	Cumulative Sum Range & \cite{kim_epoch_2014}
	\cite{kim_package_2016} \\
	
	Density Score & \cite{faraway_modeling_2016} \\
	
	Excess Abbe Value & \cite{sokolovsky_comparative_2017} \\
	
	Excursions & \cite{sokolovsky_comparative_2017} \\
	
	Flux Percentage Ratio & \cite{richards_machine-learned_2011}
	\cite{disanto_analysis_2016} \\
	
	Fourier Amplitude & \cite{kim_package_2016} \\
	
	Interquartile Range & \cite{kim_epoch_2014}
	\cite{kim_package_2016}
	\cite{sokolovsky_comparative_2017} \\
	
	Kurtosis & \cite{kim_epoch_2014}
	\cite{kim_package_2016} \\
	
	Lag-1 Autocorrelation & \cite{sokolovsky_comparative_2017} \\
	
	Linear Trend & \cite{disanto_analysis_2016} \\
	
	Magnitude Ratio & \cite{disanto_analysis_2016} \\
	
	Maximum Derivative & \cite{richards_machine-learned_2011}
	\cite{disanto_analysis_2016}
	\cite{faraway_modeling_2016} \\
	
	Maximum Successive Distance & \cite{faraway_modeling_2016} \\
	
	Mean of Successive Absolute Distances & \cite{faraway_modeling_2016} \\
	
	Mean I-V Color & \cite{dubath_random_2011} \\
	
	Mean Peak-to-Peak Slope & \cite{dubath_random_2011} \\
	
	Median Absolute Deviation & \cite{richards_machine-learned_2011}
	\cite{dubath_random_2011}
	\cite{disanto_analysis_2016}
	\cite{sokolovsky_comparative_2017} \\
	
	Median Buffer Range Percentage & \cite{richards_machine-learned_2011}
	\cite{disanto_analysis_2016} \\
	
	Non-QSO & \cite{richards_machine-learned_2011} \\
	
	Normalized Excess Variance & \cite{sokolovsky_comparative_2017} \\
	
	Pair Slope Trend & \cite{richards_machine-learned_2011}
	\cite{disanto_analysis_2016} \\
	
	Peak-to-Peak Variability & \cite{dubath_random_2011}
	\cite{sokolovsky_comparative_2017} \\
	
	Percent Amplitude & \cite{richards_machine-learned_2011}
	\cite{disanto_analysis_2016} \\
	
	Percent Difference Flux Percentile & \cite{richards_machine-learned_2011} \\
	
	Period & \cite{dubath_random_2011}
	\cite{kim_epoch_2014}
	\cite{kim_package_2016}
	\cite{disanto_analysis_2016} \\
	
	Period Signal-to-Noise Ratio & \cite{kim_epoch_2014}
	\cite{kim_package_2016} \\

	Phase$_2$ & \cite{dubath_random_2011} \\
	
	$\phi_{21}$ Phase Difference & \cite{kim_epoch_2014}
	\cite{kim_package_2016} \\
	
	$\phi_{31}$ Phase Difference & \cite{kim_epoch_2014}
	\cite{kim_package_2016} \\

	QSO & \cite{richards_machine-learned_2011} \\
	
	Quadratic Variation & \cite{faraway_modeling_2016} \\
	
	$R_{21}$ Amplitude Ratio & \cite{dubath_random_2011}
	\cite{kim_epoch_2014}
	\cite{kim_package_2016} \\
	
	$R_{31}$ Amplitude Ratio & \cite{kim_epoch_2014}
	\cite{kim_package_2016} \\
	
	R Cor Bor & \cite{disanto_analysis_2016} \\
	
	Residual Bright-Faint Ratio & \cite{kim_package_2016} \\

	Residual Scatter & \cite{dubath_random_2011} \\
	
	Robust Median Statistic & \cite{sokolovsky_comparative_2017} \\
	
	Shapiro-Wilk Normality Test & \cite{kim_package_2016} \\
	
	Skew & \cite{richards_machine-learned_2011}
	\cite{dubath_random_2011}
	\cite{kim_epoch_2014}
	\cite{kim_package_2016}
	\cite{disanto_analysis_2016} \\
	
	Slopes $10\%$ Percentile & \cite{kim_package_2016} \\
	
	Slopes $90\%$ Percentile & \cite{kim_package_2016} \\
	
	Small Kurtosis & \cite{richards_machine-learned_2011}
	\cite{disanto_analysis_2016} \\
	
	Standard Deviation & \cite{richards_machine-learned_2011}
	\cite{kim_epoch_2014}
	\cite{disanto_analysis_2016}
	\cite{sokolovsky_comparative_2017} \\
	
	Stetson's J,K, and L Variability Indices & \cite{richards_machine-learned_2011}
	\cite{kim_epoch_2014}
	\cite{kim_package_2016}
	\cite{sokolovsky_comparative_2017} \\
	
	Stetson's Variability Indices with Time-based Weighting & \cite{sokolovsky_comparative_2017} \\
	
	Stetson's Variability Indices with Limit on Magnitude Difference & \cite{sokolovsky_comparative_2017} \\
	
	Total Variation & \cite{faraway_modeling_2016} \\
	
	Variability Detection Statistic & \cite{sokolovsky_comparative_2017} \\
	
	Von Neumann Ratio & \cite{kim_epoch_2014}
	\cite{kim_package_2016}
	\cite{sokolovsky_comparative_2017} \\
	
	Welch-Stetson Variability Index & \cite{sokolovsky_comparative_2017} \\
	
	$\chi^2$ test & \cite{sokolovsky_comparative_2017} \\
\end{longtable}

\section{Amplitude (ampl)}
Amplitude is a metric which measures the difference between the maximum and minimum magnitude of a light curve. However, there are several different variations on amplitude that can be used.

One way that amplitude can be calculated is by simply taking the maximum magnitude and subtracting the minimum magnitude.

\begin{equation}
ampl = mag_{max} - mag_{min}
\end{equation}

Another variant of amplitude takes the mean of the maximum and minimum magnitude \cite{disanto_analysis_2016}.\footnote{The D'Isanto et al. paper is somewhat contradictory in its definition of this metric. It is described as using mean, however the equation they give subtracts the minimum magnitude from the maximum rather than adding them. $ampl = \frac{mag_{max} - mag_{mins}}{2}$}

\begin{equation}
ampl = \frac{mag_{max} + mag_{min}}{2}
\end{equation}

%Amplitude (ampl): ``the arithmetic average between the maximum and minimum magnitude'' %\cite{disanto_analysis_2016}
%$$
%ampl = \frac{mag_{max} - mag_{min}}{2}
%$$

%\begin{verbatim}
%def amplitude(curve):
%    max = curve.max()
%    min = curve.min()
%
%    return (max - min) / 2.0
%\end{verbatim}
%\nobibentry{disanto_analysis_2016}
\subsection{Dependencies}
\begin{itemize}
	\item Magnitudes
\end{itemize}

\subsection{Literature}
\cite{disanto_analysis_2016}


\section{Mean Crosses}
Mean crosses is a metric which measures the number of large fluctuations in a light curve. It works by calculating the number of times that the observations pass over the mean magnitude with some threshold.

There are two algorithms for calculating mean crosses, one iterates over the observations and keeps track of the last threshold passed over, while the other uses a much faster vectorized approach.

The iterative algorithm for mean crosses is the following.

\begin{enumerate}
	\item Calculate the mean and standard deviation of the magnitude.
	\item Calculate the upper and lower threshold values by adding and subtracting $0.1$ standard deviations from the mean, respectively.
	\item Check which side of the mean magnitude that the first observation is on and record it as being the last threshold crossed.
	\item For each observation, check if it has passed the threshold opposite the last passed threshold. If so, then increment a count and change the last passed threshold.
	\item Return the count.
\end{enumerate}

The vectorized algorithm for mean crosses is the following.

\begin{enumerate}
	\item Calculate the mean and standard deviation of the magnitude.
	\item Calculate the upper and lower threshold values by adding and subtracting $0.1$ standard deviations from the mean, respectively.
	\item Filter the observations down to just the ones above the upper threshold and below the lower threshold.
	\item Perform element-wise division on the remaining observations by the mean magnitude.
	\item Take two subsets of the remaining observations, the first subset containing indexes $0$ to $n - 1$, and the second containing indexes $1$ to $n$. Where $n$ is the number of remaining observations.
	\item Subtract the two subsets.
	\item Return the number of negative numbers in the resulting list.
\end{enumerate}

\subsection{Dependencies}
\begin{itemize}
	\item Magnitudes
\end{itemize}


\bibliographystyle{acm}
\bibliography{references}

\end{document}          
